import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ClickMeComponent } from './click-me.component';
import { AppComponent } from './app.component';
import {KeyUpComponent_v1,KeyUpComponent_v2,KeyUpComponent_v3,KeyUpComponent_v4,

} from './keyup.components';
import { LittleTourComponent } from './little-tour.component';
import { LoopbackComponent } from './loop-back.component';
@NgModule({
  declarations: [
    AppComponent,
    ClickMeComponent,
    KeyUpComponent_v1,
    KeyUpComponent_v2,
    KeyUpComponent_v3,
    KeyUpComponent_v4,
    LittleTourComponent,
    LoopbackComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
